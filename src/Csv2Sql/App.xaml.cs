﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading; 
using Csv2Sql.Core;
using Csv2Sql.Common;

namespace Csv2Sql
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private bool _isExit;

        public App()
        {

            //WPF Single Instance Application
            var process = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
            if (process.Length > 1)
            {
                MessageBox.Show(
                    "Csv2Sql is already running ...",
                    "Csv2Sql",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
                this.Shutdown();
            }

            this.Startup += App_Startup; ;
            this.Exit += App_Exit; ;
            this.DispatcherUnhandledException += appDispatcherUnhandledException;
        }

        void InstallMeOnStartUp() {
            try {
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                Assembly curAssembly = Assembly.GetExecutingAssembly();
                key.SetValue(curAssembly.GetName().Name, curAssembly.Location);
            } catch {}
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            this.Shutdown();
        }


        private void App_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Any())
            {
                this.Properties["StartupFileName"] = e.Args[0];
            }

            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.DoubleClick += ShowMainWindow;
            _notifyIcon.Icon = Csv2Sql.Properties.Resources.icon;
            _notifyIcon.Visible = true;
            _notifyIcon.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            _notifyIcon.ContextMenuStrip.Items.Add("MainWindow...").Click += ShowMainWindow;
            _notifyIcon.ContextMenuStrip.Items.Add("Exit").Click += App_Click;
        }

        private void App_Click(object sender, EventArgs e)
        {
            App_Exit(sender, e as ExitEventArgs);
        }
        private void ShowMainWindow(object sender, EventArgs e)
        {
            if (MainWindow.IsVisible)
            {
                if (MainWindow.WindowState == WindowState.Minimized)
                {
                    MainWindow.WindowState = WindowState.Normal;
                }
                MainWindow.Activate();
            }
            else
            {
                MainWindow.Show();
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!_isExit)
            {
                e.Cancel = true;
                MainWindow.Hide(); // A hidden window can be shown again, a closed one not
            }
        }


        private static void appDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionLogger.LogExceptionToFile(e.Exception);
            LogWindow.AddMessage(LogType.Error, e.Exception.Message);
            e.Handled = true;
        }
        static void currentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;
            ExceptionLogger.LogExceptionToFile(ex);
        }
        private static void createFileAssociation()
        {
            var appPath = Assembly.GetExecutingAssembly().Location;
            FileAssociation.CreateFileAssociation(".csv", "CSV", "comma-separated values File",
                appPath
            );
        }
    }
}
