﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Csv2Sql.Common; 

namespace Csv2Sql
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IObservable<long> observable = Observable.Interval(TimeSpan.FromMinutes(5));
        private Database DB;
        private string filesLocation;

        public MainWindow()
        {
            InitializeComponent();

            LoadConfig();

            DB  = new Database($@"Server={DbServer.Text};Database={DbServerDbName.Text};User Id={DbServerUsername.Text}; Password={DbServerPassword.Text};");
            //DB  = new Database($@"Server=(localdb)\MSSQLLocalDB;Database=Monitor;User Id=sa; Password=123;");
            filesLocation = FilesLocation.Text;


            observable.Subscribe(ReadFile); 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }
        
        void SaveInDB(params ParamItem[] @params){

            int result =
                DB.RunQuery($@"INSERT INTO Khalijfars(str_PowerPlant, str_Unit, str_Date, str_Time, dbl_CO2, dbl_CO, dbl_NO, dbl_NO2, dbl_O, db_SO2, str_CurrentFuel)
                                VALUES(@PowerPlant, @Unit, @Date, @Time, @CO2, @CO, @NO, @NO2, @O, @SO2, @CurrentFuel)",
                    @params);
        }
         

        void LoadConfig()
        {
            PowerPlant.Text = ConfigSetGet.GetConfigData("PowerPlant");
            Unit.Text = ConfigSetGet.GetConfigData("Unit");
            FilesLocation.Text = ConfigSetGet.GetConfigData("FilesLocation");
            

            DbServer.Text = ConfigSetGet.GetConfigData("DbServer"); 
            DbServerDbName.Text = ConfigSetGet.GetConfigData("DbServerDbName"); 
            DbServerUsername.Text = ConfigSetGet.GetConfigData("DbServerUsername"); 
            DbServerPassword.Text = ConfigSetGet.GetConfigData("DbServerPassword"); 
        }

        private void CheckAndSave_Click(object sender, RoutedEventArgs e)
        {
            if (SqlConnectionIsAlive())
            {
                SaveConfig();
                MessageBox.Show("تنظیمات با موفقیت ذخیره شدند.");
            }
            else
            {
                MessageBox.Show("ارتباط برقرار نشد.");
            }
        }

        void SaveConfig()
        {
            ConfigSetGet.SetConfigData("FilesLocation", FilesLocation.Text);
            ConfigSetGet.SetConfigData("DbServer", DbServer.Text);
            ConfigSetGet.SetConfigData("DbName", DbServerDbName.Text);
            ConfigSetGet.SetConfigData("DbServerUsername", DbServerUsername.Text);
            ConfigSetGet.SetConfigData("DbServerPassword", DbServerPassword.Text); 
        }
        
        bool SqlConnectionIsAlive()
        {
            try
            {
                return DB.RunQuery($@"SELECT 1") == -1;
            }
            catch (Exception e) {}
            return false;
        }

        void ReadFile(long item)
        { 
            var lines = new List<string>();
            var fileName = GetFileName(filesLocation);

            var filePath = System.IO.Path.Combine(filesLocation, fileName);
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                while (!sr.EndOfStream)
                { 
                    var line = sr.ReadLine();
                    if(!string.IsNullOrWhiteSpace(line)) lines.Add(line); 
                }
            }

            var lastLine = lines.Last().Split(new[]{";;"}, StringSplitOptions.None);

            SaveInDB(
                new ParamItem { ParamName = "@PowerPlant", ParamValue = "نام " },
                new ParamItem { ParamName = "@Unit", ParamValue = "Unit5" },
                new ParamItem { ParamName = "@Date", ParamValue = lastLine[0] },
                new ParamItem { ParamName = "@Time", ParamValue = lastLine[1] },
                new ParamItem { ParamName = "@CO2", ParamValue = lastLine[2] },
                new ParamItem { ParamName = "@CO", ParamValue = lastLine[3] },
                new ParamItem { ParamName = "@NO", ParamValue = lastLine[4] },
                new ParamItem { ParamName = "@NO2", ParamValue = lastLine[5] },
                new ParamItem { ParamName = "@O", ParamValue = lastLine[6] },
                new ParamItem { ParamName = "@SO2", ParamValue = lastLine[7] },
                new ParamItem { ParamName = "@CurrentFuel", ParamValue = lastLine[8] }
            );

        }

        string GetFileName(string path)
        {
            var directory = new DirectoryInfo(path);
            var files = directory.GetFiles("*.csv", SearchOption.TopDirectoryOnly);
            return files.OrderByDescending(i => i.LastWriteTime).First().Name;
        }


    }
    
    class TimeIndex
    {
        public TimeIndex(int index, DateTimeOffset time)
        {
            Index = index;
            Time = time;
        }
        public int Index { get; set; }
        public DateTimeOffset Time { get; set; }
    }
}
