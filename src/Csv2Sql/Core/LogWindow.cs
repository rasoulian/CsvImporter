﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csv2Sql.Models;

namespace Csv2Sql.Core
{
    public enum LogType
    {
        Alert,
        Error,
        Info,
        Announcement
    }

    public class LogWindow
    {
        public static void AddMessage(LogType type, string msg)
        {
            MVVM.App.Messenger.NotifyColleagues("AddLog",
                new Log
                {
                    Message = msg,
                    Type = type.ToString(),
                    Time = DateTime.Now
                }
            );
        }
    }
}
