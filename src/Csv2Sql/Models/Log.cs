﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csv2Sql.Models
{
    public class Log
    {
        public string Type { set; get; }
        public DateTime Time { set; get; }
        public string Message { set; get; }
    }
}
