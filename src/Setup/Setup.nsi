!include "FileAssociation.nsh"

; The name of the installer
Name "Csv2Sql 0.1"

; The file to write

OutFile "Csv2Sql_0.1_setup.exe"
Caption "Csv2Sql 0.1 Installer"
XPStyle on
SetDatablockOptimize on
CRCCheck force

SetCompressor /SOLID /FINAL lzma

; The default installation directory
InstallDirRegKey HKLM "Software\Csv2Sql" "folder"
Icon "${NSISDIR}\Contrib\Graphics\Icons\orange-install.ico"

;--------------------------------

; Pages
Page directory
Page instfiles

ShowInstDetails show
AutoCloseWindow true
;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR      
  SetOverwrite try ; NOT AN INSTRUCTION, NOT COUNTED IN SKIPPINGS  
  
 
  File "..\Csv2Sql\bin\Release\Csv2Sql.exe.config"
  File "..\Csv2Sql\bin\Release\Csv2Sql.exe" 
  
  WriteRegStr HKLM "Software\Csv2Sql" "folder" $INSTDIR
  
  ;create desktop shortcut
  CreateShortCut "$DESKTOP\SubtitleTools.lnk" "$INSTDIR\Csv2Sql.exe" ""
  
  CreateShortCut "$SMSTARTUP\Csv2Sql.lnk" "$INSTDIR\Csv2Sql.exe" ""
  
  ;Set File Association
  ${registerExtension} "$INSTDIR\Csv2Sql.exe" ".csv" "Subtitle_File"
  
  MessageBox MB_OK "You have successfully installed Csv2Sql. Please use the desktop icon to start the program."
  
SectionEnd ; end the section

